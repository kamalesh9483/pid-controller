clear;
clc;

% Receiving OLTF system equation from user as string
s = tf('s')
disp("Enter OLTF in S domain, mentioning as s \n")
OLTF = input('s')

% Extracting Numerator (num) and Denominator (den) terms from OLTF
[num ,den] = tfdata(OLTF)
H = tf(num,den);
% disp(H)

% Evaluating PID TF
Kp = input("Enter the value of Proportional constant Kp")
Ki = input("Enter the value of Integral constant")
Kd = input("Enter the value of Derivative constant")
C = pid(Kp,Ki,Kd)

% Finding OLTF of Compensated system .ie., C*OLTF
OLTF_comp = C * H

% Receiving feedback from the user
F = input('Enter the feedback, ex. 1 for unity'); 

% Creating CLTF using feedback()
CLTF = feedback(OLTF_comp, F)

% Receiving input response
z = tf('s')
disp("Enter input response in the format of z \n")
input_res = input('s')

% Output response of compensated system
y = CLTF * input_res

% Extracting Numerator (num) and Denominator (den) terms from OLTF
[num ,den] = tfdata(y)
syms Equ
num_s = poly2sym(num, Equ)
den_s = poly2sym(den, Equ)

% Taking Inverse mnb Laplace Transform of Output response C(s) 
% To convert to Output response to time domain
T = ilaplace(num_s/den_s)

% Plot for Compensated system
fplot(T, [0 2], 'b')
ylabel('Output time response ')
xlabel('Time, t in sec')
hold off
grid on